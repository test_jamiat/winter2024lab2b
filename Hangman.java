import java.util.Scanner;
public class Hangman{
	public static int isLetterInWord(String word, char c){
	//char c is the LETTER the player GUESSES
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == toUpperCase(c)){
				return i;
			}
		}
		return -1;
	}
	
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
	}
	
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String theword = "";
		if(letter0){
			theword += toUpperCase(word.charAt(0));
		}
		if(!(letter0)){
			theword += "_";  
		}
		if(letter1){
			theword += toUpperCase(word.charAt(1));
		}
		if(!(letter1)){
			theword += "_";
		}
		
		if(letter2){
			theword += toUpperCase(word.charAt(2));
		}	
		if(!(letter2)){
			theword += "_";
		}
		if(letter3){
			theword += toUpperCase(word.charAt(3));
		}
		if(!(letter3)){
			theword += "_";
		}
		System.out.println(theword);
	}
	
	public static void runGame(String word){
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		System.out.println("You have 6 misses");
		//word = word.toUpperCase();
		
		int count = 6; 
		// if the elements in this is true, it runs again
		while((letter0 == true && letter1 == true && letter2 == true && letter3 == true) || count > 0){
			//System.out.println("You have 6 misses");
			System.out.println("Please input your next guess");
			
			Scanner reader=new Scanner(System.in);
			char guess = reader.nextLine().charAt(0);
			toUpperCase(guess);
			
			int index = isLetterInWord(word, guess);
			if(index == 0) {
				letter0 = true;
			}
			if(index == 1) {
				letter1 = true;
			}
			if(index == 2) { 
				letter2 = true;
			}
			if(index == 3) {
				letter3 = true;
			}
			if(index == -1){
			count--;
			}
			else{
				count = count + 0;
			}
			System.out.println("You have " + count + " guesses left!");
			printWork(word, letter0, letter1, letter2, letter3);
			if(letter0 == true && letter1 == true && letter2 == true && letter3 == true){
				letter0 = false;
				letter1 = false;
				letter2 = false;
				letter3 = false;
				count = -1;
				System.out.println("You have successfully guessed the word " + word);
			}
			else{
				if(count == 0){
					System.out.println("YOU HAVE REACHED YOUR GUESS LIMIT. TRY AGAIN IN YOUR NEXT LIFE!!!");
				}
			}
		}
	}
	
	public static void readHangman() {
		Scanner reader=new Scanner(System.in);
		System.out.println("Please input a 4 letter word");
		String word = reader.nextLine();
		word = word.toUpperCase();
		runGame(word);
	}
}