import java.util.Scanner;
import java.util.Random;
public class wordle{
	//public static void main(String[] args){
		
		//System.out.println(randomWord.toUpperCase());
		//int count = 0;
		//while (count < 20){
		//String randomWord = generateWord();
		//runGame(randomWord.toUpperCase());
		//count++;
		//}
	//}
	
	public static String generateWord(){
		String[] words = {"truce", "brown", "brawl", "brand", "alert", "enjoy", "bread", "drugs", "chair", "table", "chain", "crowd", "crown", "kings", "break", "route", "crash", "clown", "vegan", "power"};
		Random random = new Random();
		int randomIndex = random.nextInt(words.length);
		String randomString = words[randomIndex];
		return randomString;
	}
	
	public static boolean letterInWord(String word, char c) {
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == c){
				return true;
			}
		}
		return false;
	}
	
	public static boolean letterInSlot(String word, char c, int pos) {
		if(letterInWord(word, c)) {
			if(word.charAt(pos) == c){
				return true;
			}
		}
		return false;
	}
	public static String[] guessWord(String answer, String guess) {
		String colour = "";
		String[] couleur = new String[5];
		for(int i =0; i< answer.length(); i++){
			char guessed = guess.charAt(i);
			boolean index = letterInSlot(answer, guessed, i);
			
			if(index == true){
				colour = "green";
			}
			if(!index){
				colour = "yellow";
			}
			if(letterInWord(answer, guessed) == false) {
				colour = "white";
			}
			couleur[i] = colour;
		}
		
		return couleur;
	}
	
	public static String presentResults(String word, String[] colours){
		String couleur = "";
		for(int i = 0; i< word.length(); i++){
			if(colours[i].equals("green")){
				couleur += "\u001B[32m" + word.charAt(i) + "\u001B[0m" ;
			}
			if(colours[i].equals("yellow")){
				couleur += "\u001B[33m" + word.charAt(i) + "\u001B[0m";
			}
			if(colours[i].equals("white")){
				couleur += "\u001B[0m" + word.charAt(i) ;
			}
		}
		return couleur;
	}
	
	public static String readGuess(){
		System.out.println("Guess the word");
		Scanner reader = new Scanner(System.in);
		String guess = reader.nextLine();
		if(guess.length() != 5){
			System.out.println("Please input a valid guess");
			guess = reader.nextLine();
			guess = guess.toUpperCase();
		} else{
			guess = guess.toUpperCase();
		}
		return guess;
	}
	
	public static void runGame(String word){
		int count = 0;
		while(count < 6){
			String guess = readGuess();
			String[] colours = new String[5];
			for(int i = 0; i < word.length(); i++){
				String[] colorFill = guessWord(word, guess);
				colours[i] = colorFill[i];
			}
			System.out.println(presentResults(guess, colours));
		    count++;
			
			if(word.equals(guess)){
				System.out.println("You win!");
				count = 7;
			}
			else if(count < 6){
			System.out.println("Try again");
			}
			if(count == 6){
				count = 7;
				System.out.println("You lose");
			}
		}
	}
}