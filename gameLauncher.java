import java.util.Scanner;
public class gameLauncher{
	public static void main(String[] args){
		System.out.println("Welcome to Game Launcher. Enter 1 to play Hangman and 2 to play Wordle");
		Scanner reader = new Scanner(System.in);
		String gameChoice = reader.nextLine();
		
		if(gameChoice.equals("1")){
			//System.out.println("in hangman");
			Hangman.readHangman();
		}
		
		if(gameChoice.equals("2")){
			String randomWord = wordle.generateWord();
			wordle.runGame(randomWord.toUpperCase());
		}
		
	}
}