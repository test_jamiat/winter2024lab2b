public class Application{
	public static void main(String[] args){
		Student case1 =  new Student("Jamiat Adeshina", 'f' , true);
		Student case2 =  new Student("Marvelous Akinla", 'f' , true);
		Student case3 =  new Student("Appy Yh", 'f' , false);
		Student case4 =  new Student("Chris Lalala", 'm' , false);
		Student case5 =  new Student("Muha Lululu", 'm' , true);
		
		/*System.out.println(case1.appreciation());
		System.out.println(case2.appreciation());
		System.out.println(case3.appreciation());
		System.out.println(case4.appreciation());
		System.out.println(case5.appreciation());*/
		
		Student[] section3 = new Student[5];
		section3[0] = case1;
		section3[1] = case2;
		section3[2] = case3;
		section3[3] = case4;
		section3[4] = case5;

		System.out.println(section3[0].name);
		System.out.println(section3[2].name);
		
	}
}